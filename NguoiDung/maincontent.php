
<div id="home">
<div id="left">
	<div id="nameca"><a>DANH MỤC THUỐC</a></div>
    <div id="contentca">
    	<div class="cttop">
        	<div class="content">
            	<ul>
                	<li><a href="">CÂY THUỐC NAM</a></li>
                	<li><a href="">CÂY THUỐC BẮC</a></li>
                	<li><a href="">CÂY THUỐC QUÝ</a></li>
                	
                </ul>
            </div>
        </div>
    </div>
    
    <div id="namehot"><a>CÂY THUỐC TÌM NHIỀU</a></div>
    <div id="contenthot">
    	<div class="cttop">
        	<div class="content">
            		<div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/caythuoc.png"  width="66px" /></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Ngải Cứu</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/caychiavoi.jpg" width="66px" /></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Cây Chìa Vôi</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/oliu.jpg" width="66px"/></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Cây Ô Liu</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/ngugiabi.jpg" width="66px"  /></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Ngũ Gia Bì</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/rautan.jpg" width="66px" /></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Rau Tần</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="float:left; width:100%; padding-top:5px; padding-bottom:10px">
                    	<table width="100%">
                        	<tr valign="top">
                            	<td width="66px"><a href=""><img src="images/lactien.jpg" width="66px"/></a></td>
                            	<td width="10px"></td>
                            	<td><a href="" style="color:#ff2d2d; font-weight:bold; ">Lạc Tiên</a><br /><br />
                                </td>
                            </tr>
                        </table>
                    </div>
                    
             </div>
        </div>
    </div>
</div>
<div id="main">
	<div id="boxslide">
    	<img src="images/slide.jpg" width="750px" height="300px" />
    </div>
    <div id="boxcategory">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        </a>
        <div class="cate2">
        	<ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div>
    
    <div id="boxcategory">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        </a>
        <div class="cate2">
        	<ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div>
    <div id="boxcategory" style="float:right; margin-right:0px">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        <div class="cate2">
        	<ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div>
    
    <div id="boxcategory">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        <div class="cate2">
        	<ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div>
    
    <div id="boxcategory">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        <div class="cate2">
            <ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div>
    <div id="boxcategory" style="float:right; margin-right:0px">
    	<div class="name"><a href="">Cây Thuốc</a></div>
    	<a href=""><div class="image"><img src="images/image2.jpg" width="190px" height="110px" /></div></a>
        <div class="cate2">
        	<ul>
            	<li><a href="">Tên Cây Thuốc: </a></li>
            	<li><a href="">Tên Khoa Học: </a></li>
            	<li><a href="">Tên Khác: </a></li>
            	<li><a href="">Họ: </a></li>
            	<li><a href="">Tác Dụng: </a></li>
            	<li><a href="">Bệnh: </a></li>
            </ul>
        </div>
    </div	
></div>
<div style="float:left; width:1000px; padding-top:10px; padding-bottom:10px; font-weight:bold; color:#ff2d2d"></div>

<div style="clear:both"></div>
</div>