$(document).ready(function() {
    //Login form
    $('a.login-window').click(function() {

        //lấy giá trị thuộc tính href - chính là phần tử "#login-box"
        var loginBox = $(this).attr('href');

        //cho hiện hộp đăng nhập trong 300ms
        $(loginBox).fadeIn("slow");

        // thêm phần tử id="over" vào cuối thẻ body
        $('body').append('<div id="over"></div>');
        $('#over').fadeIn(300);

        $('#btn-login').click(function(){
            if($('#username').val() == "" || $('#password').val() == ""){
                alert('dien day du thong tin vao');
                if($('#username').val() == ""){$('#username').focus();}
                else{$('#password').focus();}
                return false;
            }else{
                return true;            
            }
        });
    });

//Register form
    $('a.register-window').click(function(){
        var registerBox = $(this).attr('href');

        //cho hiện hộp đăng nhập trong 300ms
        $(registerBox).fadeIn("slow");

        // thêm phần tử id="over" vào cuối thẻ body
        $('body').append('<div id="over"></div>');
        $('#over').fadeIn(300);

        $('form').submit(function ()
            {
                // Xóa trắng thẻ div show lỗi
                $('#showerror').html('');
             
                var username = $('#username1').val();
                var email = $('#email').val();
                 
                // Kiểm tra dữ liệu có null hay không
                if ($.trim(username) == ''){
                    alert('Bạn chưa nhập tên đăng nhập');
                    return false;
                }
                 
                if ($.trim(email) == ''){
                    alert('Bạn chưa nhập email');
                    return false;
                }
                 
                // Nếu bạn thích có thể viết thêm hàm kiểm tra định dang email
                // ở đây tôi làm chú yêu chỉ cách dùng ajax nên sẽ ko đề cập tới,
                // vì sợ bài dài sẽ rối
                 
                $.ajax({
                    url : 'register.php',
                    type : 'post',
                    dataType : 'json',
                    data : {
                        username : username,
                        email : email
                    },
                    success : function (result)
                    {
                        // Kiểm tra xem thông tin gửi lên có bị lỗi hay không
                        // Đây là kết quả trả về từ file do_validate.php
                        if (!result.hasOwnProperty('error') || result['error'] != 'success')
                        {
                            alert('Có vẻ như bạn đang hack website của tôi');
                            return false;
                        }
                         
                        var html = '';
                         
                        // Lấy thông tin lỗi username
                        if ($.trim(result.username) != ''){
                            html += result.username + '<br/>';
                        }
                         
                        // Lấy thông tin lỗi email
                        if ($.trim(result.email) != ''){
                            html += result.email;
                        }
                        
                        // Cuối cùng kiểm tra xem có lỗi không
                        // Nếu có thì xuất hiện lỗi
                        if (html != ''){
                            $('#showerror').append(html);
                        }
                        else {
                            // Thành công
                            $('#showerror').append('Thêm thành công');
                        }
                    }
                });
                 
                return false;
            });
    });

    // khi click đóng hộp thoại
    $(document).on('click', "a.close, #over", function() { 
        $('#over, .login').fadeOut(300 , function() {
            $('#over').remove();  
        }); 
    });
});


