
<?php
session_start();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Trang người quản lý</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	
	<body>
        <?php
  
     if(isset($_SESSION['username'])){
		 echo $_SESSION['username'];
			include("modules/config.php");
			include("modules/banner.php");
			include("modules/menu.php");
			include("modules/content.php");
			include("modules/footer.php");
	 }
	 else {
		header("Location: http://localhost:1235/Khach/khach.php");
	 }
         ?>
		
	</body>
</html>
