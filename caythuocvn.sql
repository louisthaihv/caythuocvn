-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2014 at 02:15 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `caythuocvn`
--

-- --------------------------------------------------------

--
-- Table structure for table `baiviet`
--

CREATE TABLE IF NOT EXISTS `baiviet` (
`maBaiViet` mediumint(9) NOT NULL COMMENT 'Mã bài viết',
  `tenBaiviet` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên bài viết',
  `tomTat` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tóm tắt',
  `anhMinhHoa` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ảnh minh họa',
  `noiDung` mediumtext COLLATE utf8_unicode_ci COMMENT 'Nội dung bài viết'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng bài viết về cây thuốc và bài thuốc' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `baiviet`
--

INSERT INTO `baiviet` (`maBaiViet`, `tenBaiviet`, `tomTat`, `anhMinhHoa`, `noiDung`) VALUES
(1, '', '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `benh`
--

CREATE TABLE IF NOT EXISTS `benh` (
`maBenh` int(11) NOT NULL COMMENT 'Mã bệnh',
  `tenBenh` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên bệnh'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng bệnh' AUTO_INCREMENT=32 ;

--
-- Dumping data for table `benh`
--

INSERT INTO `benh` (`maBenh`, `tenBenh`) VALUES
(1, 'Bệnh phụ nữ'),
(2, 'Mụn nhọt, mẩn ngứa'),
(3, 'Giun sán'),
(4, 'Kiết lỵ'),
(5, 'Thông tiểu tiện, thông mật'),
(6, 'Cầm máu'),
(7, 'Hạ huyết áp'),
(8, 'Cây thuốc có độc'),
(9, 'Tiêu hóa'),
(30, 'ung thu'),
(11, 'Nhuận tràng, tẩy'),
(12, 'Đau dạ dày'),
(13, 'Tê thấp, đau nhức'),
(14, 'Đắp thương do rắn rết cắn'),
(15, 'Chữa mắt, tai, mũi, răng, họng'),
(16, 'Bệnh tim'),
(17, 'Cảm sốt'),
(18, 'Ho hen'),
(19, 'An thần, trấn kinh, ngủ'),
(20, 'Bổ, bồi dưỡng'),
(21, 'Thuốc bổ nguồn gốc động vật'),
(22, 'Thuốc khác nguồn gốc động vật'),
(23, 'Khoáng vật'),
(24, ''),
(25, 'ung thu'),
(26, 'Dau da day'),
(27, 'dau mat hot'),
(28, 'Dau mat'),
(29, 'Dau matÆ°qw'),
(31, 'Ä‘au máº¯t Ä‘á»');

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

CREATE TABLE IF NOT EXISTS `binhluan` (
  `maNguoiDung` int(11) NOT NULL COMMENT 'Người bình luận',
  `ngayBinhLuan` date NOT NULL COMMENT 'Ngày bình luận',
  `noiDung` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nội dung bình luận'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng bình luận';

-- --------------------------------------------------------

--
-- Table structure for table `caythuoc`
--

CREATE TABLE IF NOT EXISTS `caythuoc` (
`maCaythuoc` int(11) unsigned zerofill NOT NULL COMMENT 'Mã cây thuốc',
  `tenCaythuoc` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên thường gọi',
  `tenKhac` varchar(256) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên khác',
  `tenKhoahoc` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Tên khoa học',
  `anh` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Đường dẫn đến ảnh cây thuốc',
  `ho` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Phân họ cây thuốc',
  `maTacdung` int(11) NOT NULL COMMENT 'Mã tác dụng',
  `maBenh` int(11) NOT NULL COMMENT 'Mã bệnh',
  `moTa` text COLLATE utf8_unicode_ci COMMENT 'Mô tả cây thuốc',
  `thuHai` text COLLATE utf8_unicode_ci COMMENT 'Phân bố, thu hái và chế biến ',
  `thanhPhanHoaHoc` text COLLATE utf8_unicode_ci COMMENT 'Thành phần hoá học ',
  `tacDungDuocLy` text COLLATE utf8_unicode_ci COMMENT 'Tác dụng dược lý ',
  `congDung` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'Công dụng và liều dùng ',
  `donThuoc` text COLLATE utf8_unicode_ci COMMENT 'Đơn thuốc '
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `caythuoc`
--

INSERT INTO `caythuoc` (`maCaythuoc`, `tenCaythuoc`, `tenKhac`, `tenKhoahoc`, `anh`, `ho`, `maTacdung`, `maBenh`, `moTa`, `thuHai`, `thanhPhanHoaHoc`, `tacDungDuocLy`, `congDung`, `donThuoc`) VALUES
(00000000001, 'adada', 'adadad', 'adadad', '', 'adadad', 0, 0, 'adadada', 'adadad', 'adadad', 'adadad', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `caythuoc-benh`
--

CREATE TABLE IF NOT EXISTS `caythuoc-benh` (
  `maCaythuoc` int(11) NOT NULL COMMENT 'Mã cây thuốc',
  `maBenh` int(11) NOT NULL COMMENT 'Mã bệnh'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng Cây thuốc - Bệnh';

-- --------------------------------------------------------

--
-- Table structure for table `caythuoc-tacdung`
--

CREATE TABLE IF NOT EXISTS `caythuoc-tacdung` (
  `maCaythuoc` int(11) NOT NULL COMMENT 'Mã cây thuốc',
  `maTacdung` int(11) NOT NULL COMMENT 'Mã tác dụng'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng Cây thuốc - Tác dụng';

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `maNguoidung` int(11) NOT NULL COMMENT 'Mã người thay đổi thông tin',
  `ngayThayDoi` date NOT NULL COMMENT 'Ngày thay đổi thông tin',
  `noiDung` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nội dung thay đổi'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng ghi chép các thay đổi thông tin';

-- --------------------------------------------------------

--
-- Table structure for table `nguoidung`
--

CREATE TABLE IF NOT EXISTS `nguoidung` (
`maNguoidung` int(11) NOT NULL COMMENT 'Mã người dùng',
  `tenDangNhap` varchar(24) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'Tên đăng nhập',
  `matKhau` varchar(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'Mật khẩu',
  `hoTen` varchar(35) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Họ tên',
  `email` varchar(128) CHARACTER SET ascii COLLATE ascii_bin NOT NULL COMMENT 'Email',
  `noiO` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nơi ở',
  `maQuyen` int(11) NOT NULL COMMENT 'Mã quyền',
  `trangThai` tinyint(1) NOT NULL COMMENT 'Trạng thái kích hoạt',
  `ngayDangKy` date NOT NULL COMMENT 'Ngày đăng ký',
  `ghiChu` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ghi chú'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng người dùng' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `nguoidung`
--

INSERT INTO `nguoidung` (`maNguoidung`, `tenDangNhap`, `matKhau`, `hoTen`, `email`, `noiO`, `maQuyen`, `trangThai`, `ngayDangKy`, `ghiChu`) VALUES
(1, 'buivuanh', 'e10adc3949ba59abbe56e057f20f883e', 'BÃ¹i VÅ© Anh', 'buivuanh@gmail.com', 'Cáº§u Giáº¥y, HÃ  Ná»™i', 4, 0, '2014-10-15', 'Quáº£n trá»‹ há»‡ thá»‘ng'),
(5, 'quanly1', '76ce09fc04225228897e61087b1172a8', 'test ', 'test@gmail.com', 'test', 3, 1, '2014-11-11', 'test1'),
(4, 'nguoidung2', 'e10adc3949ba59abbe56e057f20f883e', 'anh', 'anh@gmail.com', 'anh', 1, 1, '2014-11-11', 'anh');

-- --------------------------------------------------------

--
-- Table structure for table `quyen`
--

CREATE TABLE IF NOT EXISTS `quyen` (
`maQuyen` int(11) NOT NULL COMMENT 'Mã quyền',
  `tenQuyen` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên quyền'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng quyền' AUTO_INCREMENT=7 ;

--
-- Dumping data for table `quyen`
--

INSERT INTO `quyen` (`maQuyen`, `tenQuyen`) VALUES
(1, 'Khách'),
(2, 'Người dùng'),
(3, 'Quản lý'),
(4, 'Quản trị'),
(5, 'test'),
(6, '');

-- --------------------------------------------------------

--
-- Table structure for table `tacdung`
--

CREATE TABLE IF NOT EXISTS `tacdung` (
`maTacdung` int(11) NOT NULL COMMENT 'Mã tác dụng',
  `tenTacdung` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên tác dụng'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng tác dụng - cây thuốc' AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baiviet`
--
ALTER TABLE `baiviet`
 ADD PRIMARY KEY (`maBaiViet`);

--
-- Indexes for table `benh`
--
ALTER TABLE `benh`
 ADD PRIMARY KEY (`maBenh`);

--
-- Indexes for table `binhluan`
--
ALTER TABLE `binhluan`
 ADD KEY `ngayBinhLuan` (`ngayBinhLuan`);

--
-- Indexes for table `caythuoc`
--
ALTER TABLE `caythuoc`
 ADD PRIMARY KEY (`maCaythuoc`), ADD UNIQUE KEY `tenCaythuoc_2` (`tenCaythuoc`), ADD KEY `maCaythuoc` (`maCaythuoc`), ADD KEY `tenCaythuoc` (`tenCaythuoc`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD KEY `ngayThayDoi` (`ngayThayDoi`);

--
-- Indexes for table `nguoidung`
--
ALTER TABLE `nguoidung`
 ADD PRIMARY KEY (`maNguoidung`);

--
-- Indexes for table `quyen`
--
ALTER TABLE `quyen`
 ADD PRIMARY KEY (`maQuyen`);

--
-- Indexes for table `tacdung`
--
ALTER TABLE `tacdung`
 ADD PRIMARY KEY (`maTacdung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `baiviet`
--
ALTER TABLE `baiviet`
MODIFY `maBaiViet` mediumint(9) NOT NULL AUTO_INCREMENT COMMENT 'Mã bài viết',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `benh`
--
ALTER TABLE `benh`
MODIFY `maBenh` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mã bệnh',AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `caythuoc`
--
ALTER TABLE `caythuoc`
MODIFY `maCaythuoc` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'Mã cây thuốc',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `nguoidung`
--
ALTER TABLE `nguoidung`
MODIFY `maNguoidung` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mã người dùng',AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `quyen`
--
ALTER TABLE `quyen`
MODIFY `maQuyen` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mã quyền',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tacdung`
--
ALTER TABLE `tacdung`
MODIFY `maTacdung` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mã tác dụng';
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
